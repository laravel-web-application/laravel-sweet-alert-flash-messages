<?php

namespace App\Http\Controllers;

use App\User;

class TestController extends Controller
{
    public function index()
    {
        $users = User::get();
        return view('user.user', compact('users'));
    }

    public function destroy($id)
    {
        User::where('id', $id)->delete();
        return back();
    }
}

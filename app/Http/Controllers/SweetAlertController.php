<?php

namespace App\Http\Controllers;
use Alert;
use App\User;
use Illuminate\Http\Request;

class SweetAlertController extends Controller
{
    public function index($alertType = null)
    {
        switch ($alertType) {
            case 'info':
                Alert::info('Welcome', 'Demo info alert');
                break;
            case 'success':
                Alert::success('Welcome', 'Demo success alert');
                break;
            case 'error':
                Alert::error('Welcome', 'Demo error alert');
                break;
            case 'warning':
                Alert::warning('Welcome', 'Demo warning alert');
                break;
            case 'success_autoclose':
                Alert::success('Welcome', 'Demo success alert')->autoclose(3500);
                break;
            case 'confirmation':
                Alert::success('Welcome', 'Demo success alert')->persistent("Ok");;
                break;
            default:
                Alert::message('Robots are working!');
                break;
        }
        return view('test');
    }

    public function myLivetoaster($type)
    {
        switch ($type) {
            case 'message':
                alert()->message('live Sweet Alert with message Good Luck.');
                break;
            case 'basic':
                alert()->basic('live Sweet Alert with basic Good Luck.', 'Basic');
                break;
            case 'info':
                alert()->info('live Sweet Alert with info Good Luck.');
                break;
            case 'success':
                alert()->success('some Sweet Alert with success Good Luck.', 'Welcome to ItSolutionStuff.com')->autoclose(4000);
                break;
            case 'error':
                alert()->error('some Sweet Alert with error Good Luck.');
                break;
            case 'warning':
                alert()->warning('some Sweet Alert with warning Good Luck.');
                break;
            default:
                # your code...
                break;
        }
        return view('my-livetoaster');
    }

    public function destroy(Request $request)
    {
        User::find($request->id)->delete();
        return response()->json(['message' => "User deleted successfully"]);
    }

}

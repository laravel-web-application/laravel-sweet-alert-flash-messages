<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>


<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Sweet Alert2
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-sweet-alert-flash-messages.git`
2. Go inside the folder: `cd laravel-sweet-alert-flash-messages`
3. Run `cp .env.example .env` then put your database name & credentials.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000/my-livetoaster/warning

### Screen shot

Basic Alert

![Basic Alert](img/basic.png "Basic Alert")

Error Alert

![Error Alert](img/error.png "Error Alert")

Success Alert

![Success Alert](img/success.png "Success Alert")

Info Alert

![Info Alert](img/info.png "Info Alert")

Warning Alert

![Warning Alert](img/warning.png "Warning Alert")

Users List Page

![Users List Page](img/user.png "Users List Page")

![Users List Page](img/delete.png "Users List Page")

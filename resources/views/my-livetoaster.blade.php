<!DOCTYPE html>
<html>
<head>
    <title>Laravel Sweet Alert Notification</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</head>
<body>
<h1 class="text-center">Laravel Sweet Alert Notification step by step</h1>
@include('sweet::alert')
<p>Live24u is the most popular Programming & Web Development blog. Our mission is to provide the best online resources
    on programming and web development. We deliver the useful and best tutorials for web professionals — developers,
    programmers, freelancers and site owners. Any visitors of this site are free to browse our tutorials, live demos and
    download scripts.</p>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script>
    $(document).ready(function () {
        $(document).on('click', '.delete', function (e) {
            e.preventDefault();
            var el = this;
            var user_id = $(this).attr("data-id");
            swal({
                    title: "Do you want to delete this User?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn",
                    confirmButtonText: "Confirm",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    timer: 5000
                },
                function (isConfirm) {
                    if (isConfirm) {

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': '{{csrf_token()}}'
                            }
                        });
                        $.ajax(
                            {
                                url: "users/delete/" + user_id,
                                type: 'delete', // replaced from put
                                dataType: "JSON",
                                data: {
                                    "id": user_id // method and token not needed in data
                                },
                                success: function (response) {
                                    // Remove row from HTML Table
                                    $(el).closest('tr').css('background', 'tomato');
                                    $(el).closest('tr').fadeOut(800, function () {
                                        $(this).remove();
                                    });
                                    swal("Deleted!", response.message, "success");
                                },
                                error: function (xhr) {
                                    console.log(xhr.responseText);
                                }
                            });
                    } else {
                        swal("Cancelled", "You Cancelled", "error");
                    }
                });
        });
    });
</script>


</html>
